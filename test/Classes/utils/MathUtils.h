//
//  MathUtils.h
//  test-mobile
//
//  Created by Thierry Berger on 01/12/2018.
//

#ifndef MathUtils_h
#define MathUtils_h

namespace MathUtils {
    float getAngleDifference(float angle1, float angle2);
}

#endif /* MathsUtils_h */
