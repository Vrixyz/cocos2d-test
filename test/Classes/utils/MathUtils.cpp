//
//  MathUtils.cpp
//  test-mobile
//
//  Created by Thierry Berger on 01/12/2018.
//

#include "MathUtils.h"

float MathUtils::getAngleDifference(float angle1, float angle2)
{
    float diffAngle = (angle1 - angle2);
    
    if(diffAngle >= 180.f)
    {
        diffAngle -= 360.f;
    }
    else if (diffAngle < -180.f)
    {
        diffAngle +=360.f;
    }
    return diffAngle;
}
