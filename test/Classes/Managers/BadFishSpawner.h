//
//  BadFishSpawner.hpp
//  test-mobile
//
//  Created by Thierry Berger on 25/11/2018.
//

#ifndef BadFishSpawner_hpp
#define BadFishSpawner_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "../Nodes/BadFish.h"

class BadFishSpawnerDelegate {
public:
    virtual void badFishReachedTarget(const BadFish& badFish, const cocos2d::Vec2& target) = 0;
};

class BadFishSpawner {
    float minSpawnRate = 0.8f;
    float maxSpawnRate = 2.6f;
    constexpr static const float minTimeToReach = 6.f;
    constexpr static const float maxTimeToReach = 10.f;
    
    cocos2d::Node* parentNode = nullptr;
    cocos2d::Vec2 targetPoint;
    
    std::vector<cocos2d::Vec2> spawnPoints;
    cocos2d::Vector<BadFish *> aliveBadFishes;
    cocos2d::Vector<BadFish *> deadBadFishes;
    float timeUntilNextSpawn = 0;
    bool isSpawningAutomatically = true;
    
public:
    BadFishSpawnerDelegate* delegate = nullptr;
    
    BadFishSpawner(std::vector<cocos2d::Vec2>&& spawnPoints, const cocos2d::Vec2 target, cocos2d::Node* parentNode);
    ~BadFishSpawner();
    void update(float dt);
    /// BadFishSpawner manages its own spawning, but you can force a spawn by calling this method.
    /// Totally NOT thread safe!
    void spawn();
    void remove(BadFish* badFish);
    void stopAutomaticSpawning();
    void increaseDifficulty();
    /// Sets the automatic spawning back to true and choose a arbitrary time to spawn next enemy.
    void resetTimeUntilNextSpawn();
    
    const cocos2d::Vector<BadFish *>& getAliveBadFishes() const;
};

#endif /* BadFishSpawner_hpp */
