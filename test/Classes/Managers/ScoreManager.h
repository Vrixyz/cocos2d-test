//
//  ScoreManager.h
//  test-mobile
//
//  Created by Thierry Berger on 27/11/2018.
//

#ifndef ScoreManager_h
#define ScoreManager_h

#include <stdio.h>
#include "cocos2d.h"
#include "network/HttpClient.h"
#include <map>
#include <string>

class ScoreManager {
    constexpr static const char* apiUrl = "https://scoreboard-redis.herokuapp.com";
    /// Scores cache, at the moment loaded only once.
    std::map<std::string, int> scores;
    bool loaded = false;
    bool loading = false;
    
    std::function<void(const std::map<std::string, int>*)> callback;
    void onHttpRequestCompleted(cocos2d::network::HttpClient* sender, cocos2d::network::HttpResponse* response);
public:
    static const char* SCORE_UPDATED_EVENT;
    cocos2d::EventDispatcher* eventDispatcher;
    /// Callback map will be not null if we succeeded in retrieving data from server.
    /// Only first successful call will be async, later, cache is used. You can force it with `forceLoad` parameter.
    /// Not thread safe. If you try to call this function before async call returned, it will fail.
    void retrieveScores(std::function<void(const std::map<std::string, int>*)> callback, bool forceLoad = false);
    void sendScore(const std::string& name, int score);
};

#endif /* ScoreManager_h */
