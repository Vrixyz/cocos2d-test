//
//  BulletPool.cpp
//  test-mobile
//
//  Created by Thierry Berger on 25/11/2018.
//

#include "BulletPool.h"

USING_NS_CC;

BulletPool::BulletPool()
{
    for (int i = 0; i < BULLET_POOL_SIZE; i++) {
        this->bullets[i] = Bubble::create();
        this->bullets[i]->retain();
    }
}

BulletPool::~BulletPool()
{
    for (int i = 0; i < BULLET_POOL_SIZE; i++) {
        this->bullets[i]->release();
    }
}

void BulletPool::spawn(cocos2d::Node& parent, Vec2 position, Vec2 target)
{
    if (this->nbBulletsAlive < BULLET_POOL_SIZE) {
        auto bulletToSpawn = this->bullets[this->nbBulletsAlive];
        // FIXME: crash here: bulletToSpawn was null.
        parent.addChild(bulletToSpawn, 1);
        bulletToSpawn->setPosition(position);
        bulletToSpawn->fire(target);
        this->nbBulletsAlive++;
    }
}

void BulletPool::removeBullet(Bubble* bulletToRemove)
{
    for (int i = 0; i < this->nbBulletsAlive; i++) {
        if (this->bullets[i] == bulletToRemove) {
            this->removeBullet(i);
            break;
        }
    }
}

void BulletPool::removeBullet(int index) {
    Bubble* bulletToRemove = this->bullets[index];
    // FIXME: thread1 crash bulletToRemove null
    bulletToRemove->removeFromParent();
    Bubble* bulletToSwap = this->bullets[this->nbBulletsAlive - 1];
    this->bullets[this->nbBulletsAlive - 1] = bulletToRemove;
    this->bullets[index] = bulletToSwap;
    this->nbBulletsAlive--;
}

void BulletPool::checkCollisions(BadFishSpawner* fishSpawner)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Rect sceneRect = Rect(origin, visibleSize);
    for (int i = this->nbBulletsAlive - 1; i >= 0; i--) {
        auto bullet = this->bullets[i];
        if (!sceneRect.containsPoint(bullet->getPosition())) {
            this->removeBullet(i);
            continue;
        }
        auto badFishes = fishSpawner->getAliveBadFishes();
        for (int j = (int)badFishes.size() - 1; j >= 0; j--) {
            auto badFish = badFishes.at(j);
            if (bullet->getBoundingBox().containsPoint(badFish->getPosition())) {
                // FIXME: thread1 crash
                this->removeBullet(i);
                if (this->delegate) {
                    this->delegate->bubbleDidKillEnemy(*badFish);
                }
                fishSpawner->remove(badFish);
                break;
            }
        }
    }
}
