//
//  ScoreManager.cpp
//  test-mobile
//
//  Created by Thierry Berger on 27/11/2018.
//

#include "ScoreManager.h"
#include "external/json/document.h"

USING_NS_CC;
using namespace network;
using namespace rapidjson;

const char* ScoreManager::SCORE_UPDATED_EVENT = "score updated";

void ScoreManager::onHttpRequestCompleted(HttpClient* client, HttpResponse* response) {
    if (!response)
    {
        return;
    }

    // Dump the data
    std::vector<char> *buffer = response->getResponseData();
    std::string buffer_s( buffer->begin( ), buffer->end( ) );
    log("response: %s\n", buffer_s.c_str());
    
    Document document;
    document.Parse<0>(buffer_s.c_str());
    if (document.HasParseError()) {
        return;
    }
    for (auto itr = document.MemberBegin();
         itr != document.MemberEnd(); ++itr)
    {
        if (itr->value.GetType() == kNumberType) {
            auto key = itr->name.GetString();
            this->scores[key]= itr->value.GetInt();
        }
    }
    this->loaded = true;
    this->callback(&this->scores);
    this->loading = false;
}

void ScoreManager::retrieveScores(std::function<void(const std::map<std::string, int>*)> callback, bool forceLoad) {
    if (!this->loaded || forceLoad) {
        log("reloading!");
        if (this->loading) {
            log("Avoid calling `retrieveScores` multiple times in a row.");
            callback(nullptr);
            return;
        }
        this->loading = true;
        this->callback = callback;
        HttpRequest* request = new (std :: nothrow) HttpRequest();
        request->setUrl(std::string(this->apiUrl) + "/get");
        request->setRequestType(HttpRequest::Type::GET);
        request->setResponseCallback(CC_CALLBACK_2 (ScoreManager::onHttpRequestCompleted, this));
        
        HttpClient::getInstance()->sendImmediate(request);
        
        request->release();
    }
    else {
        callback(&this->scores);
    }
}

void ScoreManager::sendScore(const std::string& name, int score) {
    HttpRequest* request = new (std :: nothrow) HttpRequest();
    request->setUrl(std::string(this->apiUrl) + "/set/" + name + "/" + std::to_string(score));
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback([this](HttpClient* client, HttpResponse* response) {
        if (this->eventDispatcher) {
            EventCustom event(SCORE_UPDATED_EVENT);
            this->eventDispatcher->dispatchEvent(&event);
        }
    });
    HttpClient::getInstance()->sendImmediate(request);
    request->release();
}
