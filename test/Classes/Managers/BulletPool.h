//
//  BulletPool.hpp
//  test-mobile
//
//  Created by Thierry Berger on 25/11/2018.
//

#ifndef BulletPool_hpp
#define BulletPool_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "../Nodes/Bubble.h"
#include "../Managers/BadFishSpawner.h"

#define BULLET_POOL_SIZE 50

class BulletPoolDelegate {
public:
    virtual void bubbleDidKillEnemy(const BadFish& badFish) = 0;
};

class BulletPool {
    /// First bullets are alive, bullets after `nbBulletsAlive` are not active.
    Bubble* bullets[BULLET_POOL_SIZE];
    int nbBulletsAlive = 0;
    
public:
    BulletPoolDelegate* delegate = nullptr;
    BulletPool();
    ~BulletPool();
    void spawn(cocos2d::Node& parent, cocos2d::Vec2 position, cocos2d::Vec2 target);
    void removeBullet(Bubble* bulletToRemove);
    void removeBullet(int index);
    void checkCollisions(BadFishSpawner* fishSpawner);
};

#endif /* BulletPool_hpp */
