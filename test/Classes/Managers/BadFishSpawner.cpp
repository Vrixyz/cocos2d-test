//
//  BadFishSpawner.cpp
//  test-mobile
//
//  Created by Thierry Berger on 25/11/2018.
//

#include "BadFishSpawner.h"

USING_NS_CC;

BadFishSpawner::BadFishSpawner(std::vector<cocos2d::Vec2>&& spawnPoints, const Vec2 target, Node* parentNode) : spawnPoints(spawnPoints){
    this->parentNode = parentNode;
    for (int i = 0; i < 100; i++) {
        this->deadBadFishes.pushBack(BadFish::create(target));
    }
    this->targetPoint = target;
    log("target: %f, %f", target.x, target.y);
}

BadFishSpawner::~BadFishSpawner() {
    
}

void BadFishSpawner::update(float dt) {
    //log("time until next spawn: %f", this->timeUntilNextSpawn);
    if (this->isSpawningAutomatically) {
        this->timeUntilNextSpawn -= dt;
        if (this->timeUntilNextSpawn <= 0) {
            this->spawn();
            this->resetTimeUntilNextSpawn();
        }
    }
    for (auto badFish : this->aliveBadFishes) {
        if (badFish->getBoundingBox().containsPoint(this->targetPoint)) {
            if (this->delegate) {
                this->delegate->badFishReachedTarget(*badFish, this->targetPoint);
            }
            this->remove(badFish);
        }
    }
}

void BadFishSpawner::spawn() {
    if (this->deadBadFishes.size() > 0) {
        auto badFishToSpawn = this->deadBadFishes.back();
        // TODO: add random
        badFishToSpawn->setPosition(this->spawnPoints[cocos2d::RandomHelper::random_int(0, (int)this->spawnPoints.size())]);
        this->parentNode->addChild(badFishToSpawn, 1);
        this->aliveBadFishes.pushBack(badFishToSpawn);
        this->deadBadFishes.popBack();
        badFishToSpawn->attack(cocos2d::RandomHelper::random_real(this->minTimeToReach, this->maxTimeToReach));
    }
}

void BadFishSpawner::remove(BadFish* badFish) {
    parentNode->removeChild(badFish);
    this->deadBadFishes.pushBack(badFish);
    this->aliveBadFishes.eraseObject(badFish);
}

void BadFishSpawner::stopAutomaticSpawning() {
    this->isSpawningAutomatically = false;
}


void BadFishSpawner::increaseDifficulty() {
    this->minSpawnRate *= 0.9f;
    this->maxSpawnRate *= 0.9f;
}

void BadFishSpawner::resetTimeUntilNextSpawn() {
    // TODO: add random
    this->timeUntilNextSpawn += cocos2d::RandomHelper::random_real(this->minSpawnRate, this->maxSpawnRate);
    this->isSpawningAutomatically = true;
}

const cocos2d::Vector<BadFish *>& BadFishSpawner::getAliveBadFishes() const {
    return this->aliveBadFishes;
}
