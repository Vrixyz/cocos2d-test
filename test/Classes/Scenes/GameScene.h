#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "../Nodes/PlayerFish.h"
#include "../Nodes/BadFish.h"
#include "../Managers/BadFishSpawner.h"
#include "../Managers/BulletPool.h"
#include "../Managers/ScoreManager.h"

class Game;

class GameDelegate {
public:
    virtual void gameDidEnd(const Game& game) = 0;
};

class Game : public cocos2d::Scene, BadFishSpawnerDelegate, PlayerFishDelegate, BulletPoolDelegate
{
    cocos2d::Label* scoreLabel;
    PlayerFish* fishSprite;
    cocos2d::EventListenerTouchOneByOne* touchListener;
    BadFishSpawner* badFishSpawner;
    BulletPool* bulletPool;
    cocos2d::Vec2* fireOnStop = nullptr;
    int score = 0;
    const std::string playerName;
    
    bool onContactBegin(cocos2d::PhysicsContact& contact);
    const BadFish* getTargetEnemy(const cocos2d::Vec2& target) const;

    void badFishReachedTarget(const BadFish& badFish, const cocos2d::Vec2& target) override;
    void playerFishFinishedRotation(PlayerFish& playerFish) override;
    void bubbleDidKillEnemy(const BadFish& badFish) override;

    void update(float dt) override;
    
public:
    Game(const std::string& playerName);
    ~Game();
    static Game* createScene(const std::string& playerName);
    GameDelegate* delegate = nullptr;
    /// Optional value, not owned by GameScene.
    ScoreManager* scoreManager = nullptr;
    
    virtual bool init() override;
    bool playerDidTouchScreen(cocos2d::Touch* touch);
    
    int getScore() const;
    void updateScoreLabel();
    
    bool isFacingAngle(float target) const;
    void fireStraightIfPossible();
};

#endif // __GAME_SCENE_H__
