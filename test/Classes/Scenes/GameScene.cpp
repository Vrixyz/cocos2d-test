#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "../Nodes/Bubble.h"
#include "../Nodes/BadFish.h"
#include "../utils/MathUtils.h"

USING_NS_CC;

Game::Game(const std::string& playerName) : Scene(), playerName(playerName)
{
    
}

Game::~Game() {
    delete(badFishSpawner);
    delete(bulletPool);
}

Game* Game::createScene(const std::string& playerName)
{
    Game *pRet = new(std::nothrow) Game(playerName);
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

// on "init" you need to initialize your instance
bool Game::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    this->scoreLabel = Label::createWithTTF("Tap on blue fishes!", "fonts/Marker Felt.ttf", 15);
    // position the label on the center of the screen
    this->scoreLabel->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - this->scoreLabel->getContentSize().height));
    
    // add the label as a child to this layer
    this->addChild(this->scoreLabel, 3);
    // add background
    auto backgroundSprite = Sprite::createWithSpriteFrameName("background.png");
    // position the sprite on the center of the screen
    backgroundSprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    backgroundSprite->setContentSize(visibleSize);
    
    // add the sprite as a child to this layer
    this->addChild(backgroundSprite, 0);
    // add player sprite
    this->fishSprite = PlayerFish::create();
    this->fishSprite->delegate = this;
    // position the sprite on the center of the screen
    this->fishSprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    // add the sprite as a child to this layer
    this->addChild(fishSprite, 2);
        
    this->touchListener = EventListenerTouchOneByOne::create();
    this->touchListener->onTouchBegan = [this](Touch* touch, Event* event){
        return this->playerDidTouchScreen(touch);
    };
    this->touchListener->onTouchMoved = [this](Touch* touch, Event* event){
        return this->playerDidTouchScreen(touch);
    };
    
    // trigger when you let up
    this->touchListener->onTouchEnded = [this](Touch* touch, Event* event){
        return this->playerDidTouchScreen(touch);
    };
    
    // Add listener
    _eventDispatcher->addEventListenerWithSceneGraphPriority(this->touchListener, this);
    
    // We could make this better by offsetting the points to let the sprite spawn outside of the screen.
    // Also, the point at which enemies are spawning could be really random by choosing a random angle, or random x,y at the border of the screen.
    auto spawnPoints = {
        origin,
        origin + visibleSize,
        Vec2(origin.x + visibleSize.width, origin.y),
        Vec2(origin.x, origin.y + visibleSize.height),
        Vec2(origin.x + visibleSize.width / 2, origin.y),
        Vec2(origin.x, origin.y + visibleSize.height / 2),
        Vec2(origin.x + visibleSize.width, origin.y + visibleSize.height / 2),
        Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height),
    };
    
    this->badFishSpawner = new BadFishSpawner(spawnPoints, this->fishSprite->getPosition(), this);
    this->badFishSpawner->delegate = this;
    
    this->bulletPool = new BulletPool();
    this->bulletPool->delegate = this;
    this->scheduleUpdate();
    
    auto sequence = RepeatForever::create(Sequence::createWithTwoActions(CallFunc::create([this](){this->badFishSpawner->increaseDifficulty(); }), DelayTime::create(10.0f)));
    runAction(sequence);
    return true;
}

bool Game::playerDidTouchScreen(Touch* touch) {
    Vec2 touchLocation = touch->getLocation();
    this->fishSprite->aim(touchLocation);
    auto enemy = this->getTargetEnemy(touchLocation);
    if (enemy != nullptr ) {
        auto currentPosition = this->fishSprite->getPosition();
        auto positionToFire = enemy->getPosition();
        float targetAngle = atan2(currentPosition.y - positionToFire.y, positionToFire.x - currentPosition.x) * 180 / M_PI;
        log("targetAngle: %f ; currentRotation: %f", targetAngle, this->fishSprite->getRotation());
        if (this->isFacingAngle(targetAngle - 90)) {
            log("fire touch");
            this->fishSprite->fire(*this->bulletPool, enemy->getPosition());
            delete(this->fireOnStop);
            this->fireOnStop = nullptr;
        }
        else {
            this->fireOnStop = new Vec2(enemy->getPosition());
        }
    }
    else {
        delete(this->fireOnStop);
        this->fireOnStop = nullptr;
    }
    return true;
}

int Game::getScore() const {
    return this->score;
}

void Game::updateScoreLabel() {
    this->scoreLabel->setString("score: " + std::to_string(this->score));
}

const BadFish* Game::getTargetEnemy(const cocos2d::Vec2& target) const {
    Vec2 currentPosition = this->fishSprite->getPosition();
    auto badFishes = this->badFishSpawner->getAliveBadFishes();
    for (auto badFish : badFishes) {
        if (badFish->getBoundingBox().containsPoint(target)) {
            return badFish;
        }
    }
    return nullptr;
}

bool Game::isFacingAngle(float angle) const {
    auto angleDifference = MathUtils::getAngleDifference(angle, this->fishSprite->getRotation());
    log("angleDifference: %f", angleDifference);
    if (fabsf(angleDifference) < 10) {
        return true;
    }
    return false;
}
void Game::update(float delta) {
    this->badFishSpawner->update(delta);
    this->bulletPool->checkCollisions(this->badFishSpawner);
}

// MARK: - delegates
void Game::badFishReachedTarget(const BadFish& badFish, const cocos2d::Vec2& target) {
    log("player was killed");
    
    if (this->scoreManager) {
        log("will update with score: %i", this->score);
        this->scoreManager->sendScore(this->playerName, this->score);
    }
    if (this->delegate) {
        this->delegate->gameDidEnd(*this);
    }
    Director::getInstance()->popScene();
}
void Game::playerFishFinishedRotation(PlayerFish& playerFish) {
    if (this->fireOnStop != nullptr) {
        log("fire on stop");
        this->fishSprite->fire(*this->bulletPool, *this->fireOnStop);
        delete(this->fireOnStop);
        this->fireOnStop = nullptr;
    }
}
void Game::bubbleDidKillEnemy(const BadFish& badFish) {
    log("Bubble did kill! score: %i", this->score);
    this->score++;
    this->updateScoreLabel();
}
