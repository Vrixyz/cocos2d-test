/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

static const char* PLAYERNAME_KEY = "playerName";

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}
// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    // add background
    cocos2d::ui::ImageView* background = cocos2d::ui::ImageView::create("menuBackground.png");
    background->setContentSize(visibleSize);
    background->ignoreContentAdaptWithSize(false);
    background->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    // position the sprite on the center of the screen
    //backgroundSprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    // add the sprite as a child to this layer
    this->addChild(background, 0);
    
    /////////////////////////////
    // 2.1 add a play button for assignment, launching game scene.
    auto labelOffset = 6;
    auto playLabel = Label::createWithSystemFont("Play", "Arial", 26);
    auto r = Rect(3.f,3.f,18.f,18.f);
    auto s_playItemNormal = cocos2d::ui::Scale9Sprite::createWithSpriteFrameName("buttonRed.png", r);
    auto s_playItemSelected = cocos2d::ui::Scale9Sprite::createWithSpriteFrameName("buttonRedPressed.png", r);
    auto labelSize = playLabel->getContentSize();
    auto buttonSize = Size(labelSize.width + labelOffset * 2, labelSize.height + labelOffset * 2);
    s_playItemNormal->setContentSize(buttonSize);
    s_playItemSelected->setContentSize(buttonSize);
    auto playItem = MenuItemSprite::create(
                                          s_playItemNormal,
                                          s_playItemSelected,
                                          CC_CALLBACK_1(HelloWorld::menuPlayCallback, this));
    playItem->addChild(playLabel);
    playLabel->setPosition(labelSize.width / 2 + labelOffset, playLabel->getContentSize().height / 2 + labelOffset);
    auto menu = Menu::create(playItem, NULL);
    menu->setPosition(visibleSize.width / 2, visibleSize.height / 2);
    this->addChild(menu, 1);
    
    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label

    auto welcomeLabel = Label::createWithTTF("Hello,  ", "fonts/Marker Felt.ttf", 24);
    welcomeLabel->setAnchorPoint(Vec2(1,0.5));
    // position the label on the center of the screen
    welcomeLabel->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - welcomeLabel->getContentSize().height));
    // add the label as a child to this layer
    this->addChild(welcomeLabel, 1);
    
    this->nameTextField = cocos2d::ui::TextField::create("World","fonts/Marker Felt.ttf",30);
    this->nameTextField->setAnchorPoint(Vec2(0, 0.5));
    this->nameTextField->setPosition(Vec2(welcomeLabel->getPosition().x,
                                          welcomeLabel->getPosition().y));
    UserDefault *def = UserDefault::getInstance();
    if (def != nullptr) {
        auto savedPlayerName = def->getStringForKey(PLAYERNAME_KEY);
        if (savedPlayerName.length() > 0) {
            this->nameTextField->setString(savedPlayerName);
        }
    }
    this->addChild(this->nameTextField);
    
    this->hintLabel = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 10);
    // position the label on the center of the screen
    this->hintLabel->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + this->hintLabel->getContentSize().height));
    this->hintLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    this->hintLabel->setWidth(visibleSize.width - 16);
    auto sequence = RepeatForever::create(Sequence::createWithTwoActions(CallFunc::create([this](){this->updateHintLabel(); }), DelayTime::create(10.0f)));
    runAction(sequence);
    this->addChild(hintLabel, 1);
    
    this->scoreboard = Scoreboard::create();
    this->scoreboard->setAnchorPoint(Vec2(0, 0.5));
    this->scoreboard->setPosition(Vec2(origin.x, origin.y + visibleSize.height / 2));
    this->scoreboard->setContentSize(Size(visibleSize.width / 4, visibleSize.height / 2));
    this->scoreboard->setScrollBarEnabled(true);
    this->scoreboard->setTouchEnabled(true);
    this->scoreboard->setBounceEnabled(true);
    this->scoreboard->setDirection(Scoreboard::ScrollView::Direction::VERTICAL);
    this->scoreboard->setPlayerName(this->getPlayerName());
    this->reloadScores();
    
    this->nameTextField->addEventListener([this](Ref*, cocos2d::ui::TextField::EventType eventType){
        if (eventType == cocos2d::ui::TextField::EventType::INSERT_TEXT ||
            eventType == cocos2d::ui::TextField::EventType::DELETE_BACKWARD) {
        this->scoreboard->setPlayerName(this->getPlayerName());
        this->reloadScores();
        this->scoreboard->reloadDisplay();
        }
    });
    
    this->addChild(this->scoreboard, 2);
        
    return true;
}

std::string HelloWorld::getPlayerName() const {
    auto name = this->nameTextField->getString();
    return name.length() > 0 ? this->nameTextField->getString() : "World";
}

void HelloWorld::reloadScores() {
    this->scoreboard->reload();
}

void HelloWorld::updateHintLabel() {
    static const char* hints[] = {
        "Something smells fishy ? Noticed a bug ? Send a mail to contact@thierryberger.com",
        "Blue fishes with long teeth are bad."
        "You are the cute orange fish.",
        "Tap the screen to rotate",
        "Tap the Blue fishes to shoot at them, aim well!",
        "Missing enemies? Bubble has to touch the exact middle of the bad blue fishes to kill them!",
        "Enemies can spawn randomly at 8 points, did you guess which ones?",
        "You, as an orange fish, are slow to rotate! Move as soon as possible to face the correct direction!",
        "You gain points at every game, try to beat your high score and climb the ladder!",
        nullptr
    };
    static int index = 0;
    if (hints[(++index)] == nullptr) {
        index = 0;
    }
    std::string label = hints[index];
    this->hintLabel->setString(label);
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    this->hintLabel->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + this->hintLabel->getContentSize().height));
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        exit(0);
    #endif

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
}

void HelloWorld::menuPlayCallback(Ref* pSender)
{
    // create a scene. it's an autorelease object
    auto name = this->nameTextField->getString();
    auto scene = Game::createScene(this->getPlayerName());
    scene->scoreManager = &this->scoreboard->scoreManager;
    scene->delegate = this;
    // run
    Director::getInstance()->pushScene(scene);
}

void HelloWorld::gameDidEnd(const Game& game) {
    log("game did end");
    UserDefault *def = UserDefault::getInstance();
    if (def != nullptr) {
        def->setStringForKey(PLAYERNAME_KEY, this->getPlayerName());
    }
}
