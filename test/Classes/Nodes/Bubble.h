//
//  Bubble.hpp
//  test-mobile
//
//  Created by Thierry Berger on 25/11/2018.
//

#ifndef Bubble_hpp
#define Bubble_hpp

#include <stdio.h>
#include "cocos2d.h"

class Bubble : public cocos2d::Sprite
{
    constexpr static float distancePerSecond = 100.f;
public:
    static Bubble* create();
    bool init() override;
    void fire(cocos2d::Vec2 target);
};

#endif /* Bubble_hpp */
