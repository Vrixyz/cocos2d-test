//
//  BadFish.hpp
//  test-mobile
//
//  Created by Thierry Berger on 25/11/2018.
//

#ifndef BadFish_hpp
#define BadFish_hpp

#include <stdio.h>
#include "cocos2d.h"

class BadFish : public cocos2d::Sprite
{
    cocos2d::MoveTo* moveAction;
    cocos2d::Vec2 target;
public:
    BadFish();
    ~BadFish();
    static BadFish* create(const cocos2d::Vec2& target);
    
    bool init(const cocos2d::Vec2& target);
    void attack(float timeToReach);
    void stop();
};

#endif /* BadFish_hpp */
