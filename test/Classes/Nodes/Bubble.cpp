//
//  Bubble.cpp
//  test-mobile
//
//  Created by Thierry Berger on 25/11/2018.
//

#include "Bubble.h"

USING_NS_CC;

Bubble* Bubble::create() {
    
    Bubble* p_Sprite = new (std::nothrow) Bubble();
    if (p_Sprite && p_Sprite->init())
    {
        p_Sprite->autorelease();
        p_Sprite->scheduleUpdate();
        return p_Sprite;
    }
    
    CC_SAFE_DELETE(p_Sprite);
    return NULL;
    
}

bool Bubble::init() {
    if (!Sprite::init()) return false;
    if (!this->initWithSpriteFrameName("bubble.png")) return false;
    
    this->setScale(0.5f);
    return true;
}

void Bubble::fire(Vec2 target) {
    
    Vec2 moveToDo = (target - this->getPosition()).getNormalized() * 10000;
    auto moveBy = MoveBy::create(moveToDo.length() / distancePerSecond, moveToDo);
    
    this->runAction(moveBy);
}
