//
//  Scoreboard.h
//  test-mobile
//
//  Created by Thierry Berger on 28/11/2018.
//

#ifndef Scoreboard_h
#define Scoreboard_h

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "../Managers/ScoreManager.h"

class Scoreboard : public cocos2d::ui::ListView
{
    // Data to display
    const std::map<std::string, int>* scores;
    std::string playerName;
public:
    ScoreManager scoreManager;
    
    Scoreboard();
    static Scoreboard* create();
    void setPlayerName(const std::string& playerName);
    /// calls server and reload display accordingly.
    void reload();
    /// reload display only
    void reloadDisplay();
};

#endif /* Scoreboard_h */
