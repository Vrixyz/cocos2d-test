//
//  PlayerSpawn.hpp
//  test
//
//  Created by Thierry Berger on 22/11/2018.
//

#ifndef PlayerSpawn_hpp
#define PlayerSpawn_hpp

#include <stdio.h>
#include "cocos2d.h"

class PlayerSpawn : public cocos2d::Node
{
protected:
    
public:    
    void spawnPlayer();
};

#endif /* PlayerSpawn_hpp */
