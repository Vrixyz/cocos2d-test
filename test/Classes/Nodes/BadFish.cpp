//
//  BadFish.cpp
//  test-mobile
//
//  Created by Thierry Berger on 25/11/2018.
//

#include "BadFish.h"

USING_NS_CC;

BadFish::BadFish() : Sprite() {

}

BadFish::~BadFish() {
    
}

BadFish* BadFish::create(const Vec2& target) {
    BadFish* p_Sprite = new (std::nothrow) BadFish();
    if (p_Sprite && p_Sprite->init(target))
    {
        p_Sprite->autorelease();
        return p_Sprite;
    }
    
    CC_SAFE_DELETE(p_Sprite);
    return NULL;
    
}

bool BadFish::init(const Vec2& target) {
    if (!Sprite::init()) return false;
    if (!this->initWithSpriteFrameName("badFish.png")) return false;
    this->target = target;
    this->setScale(0.5f);
    return true;
}

void BadFish::attack(float timeToReach) {
    this->stop();
    auto currentPosition = this->getPosition();
    float targetAngle = atan2(currentPosition.y - this->target.y, this->target.x - currentPosition.x) * 180 / M_PI;
    targetAngle -= 90;
    this->setRotation(targetAngle);
    log("attacking %f, %f", this->target.x, this->target.y);
    this->moveAction = MoveTo::create(timeToReach, this->target);
    this->runAction(this->moveAction);
}
void BadFish::stop() {
    this->stopAllActions();
}
