//
//  PlayerFish.cpp
//  test-mobile
//
//  Created by Thierry Berger on 23/11/2018.
//

#include "PlayerFish.h"
#include "../utils/MathUtils.h"

USING_NS_CC;

PlayerFish::PlayerFish() : Sprite() {
    this->initWithSpriteFrameName("fish.png");
    this->setScale(0.5f);
    cocos2d::Vector<SpriteFrame *> fireFrames;
    auto spritecache = SpriteFrameCache::getInstance();
    this->fireFrames.pushBack(spritecache->getSpriteFrameByName("fishBubble.png"));
    this->fireFrames.pushBack(spritecache->getSpriteFrameByName("fish.png"));
    auto fireAnimation = Animation::createWithSpriteFrames(this->fireFrames, 1.0f/2.f);
    cocos2d::Vector<SpriteFrame *> idleFrames;
    this->idleFrames.pushBack(spritecache->getSpriteFrameByName("fish.png"));
    auto idleAnimation = Animation::createWithSpriteFrames(this->idleFrames, 1.0f);
    
    this->runAction(RepeatForever::create(Animate::create(idleAnimation)));
    log("ok");
}

PlayerFish::~PlayerFish() {
    
}

PlayerFish* PlayerFish::create() {
    
    PlayerFish* p_Sprite = new (std::nothrow) PlayerFish();
    if (p_Sprite)
    {
        p_Sprite->autorelease();
        p_Sprite->scheduleUpdate();
        return p_Sprite;
    }
    
    CC_SAFE_DELETE(p_Sprite);
    return NULL;
    
}

void PlayerFish::aim(const cocos2d::Vec2& target) {
    
    this->command.touch = target;
    this->command.isProcessed = false;
}

void PlayerFish::fire(BulletPool &bulletPool, const cocos2d::Vec2& target) {
    if (this->currentReloadTime <= 0) {
        log("fire!");
        this->stopAllActions();
        log("runaction");
        this->runAction(Sequence::create(
                                         Repeat::create(Animate::create(this->createFireAnimation()), 1),
                                         RepeatForever::create(Animate::create(this->createIdleAnimation())), NULL));
        log("over");
        bulletPool.spawn(*this->getParent(), this->getPosition(), target);
        this->currentReloadTime = this->fireRate;
    }
}

void PlayerFish::update(float delta) {
    Sprite::update(delta);
    this->updateRotation(delta);
    this->currentReloadTime -= delta;
}

void PlayerFish::updateRotation(float delta) {
    if (!this->command.isProcessed) {
        this->command.isProcessed = true;
        Vec2 target = this->command.touch;
        float currentRotation = (this->getRotation());
        if (currentRotation < -360) {
            currentRotation += 360;
        }
        else if (currentRotation > 360) {
            currentRotation -= 360;
        }
        this->setRotation(currentRotation);
        auto currentPosition = this->getPosition();
        float targetAngle = atan2(currentPosition.y - target.y, target.x - currentPosition.x) * 180 / M_PI;
        targetAngle = currentRotation - MathUtils::getAngleDifference(currentRotation + 90, targetAngle);

        this->angleTarget = targetAngle;
    }
    float currentRotation = this->getRotation();
    if (currentRotation != this->angleTarget) {
        float direction = this->angleTarget > currentRotation ? 1 : -1;
        float maxRotation = this->angleTarget - currentRotation;
        float newAngle;
        if (this->rotationSpeed * delta > fabsf(maxRotation)) {
            newAngle = angleTarget;
        }
        else {
            newAngle = currentRotation + this->rotationSpeed * delta * direction;
        }
        this->setRotation(newAngle);
        if (newAngle == this->angleTarget) {
            this->setRotation(newAngle);
            this->angleTarget = newAngle;
            if (this->delegate) {
                this->delegate->playerFishFinishedRotation(*this);
            }
        }
    }
}

cocos2d::Animation* PlayerFish::createFireAnimation() const
{
    return Animation::createWithSpriteFrames(this->fireFrames, this->fireRate / 2);
}
cocos2d::Animation* PlayerFish::createIdleAnimation() const
{
    return Animation::createWithSpriteFrames(this->idleFrames, 1.0f);;
}
