//
//  Scoreboard.cpp
//  test-mobile
//
//  Created by Thierry Berger on 28/11/2018.
//

#include "Scoreboard.h"

USING_NS_CC;

Scoreboard::Scoreboard() : ListView::ListView() {
    auto listener = EventListenerCustom::create(ScoreManager::SCORE_UPDATED_EVENT, [this](EventCustom* event) {
        this->reload();
    });
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    this->scoreManager.eventDispatcher = _eventDispatcher;
}

Scoreboard* Scoreboard::create() {
    Scoreboard* scoreboard = new Scoreboard();
    if (scoreboard && scoreboard->init())
    {
        scoreboard->autorelease();
        return scoreboard;
    }
    CC_SAFE_DELETE(scoreboard);
    return nullptr;
}

void Scoreboard::setPlayerName(const std::string& playerName) {
    this->playerName = playerName;
}

void Scoreboard::reload() {
    this->scoreManager.retrieveScores([this](const std::map<std::string, int>* scores) -> void {
        if (!scores) {
            return;
        }
        this->scores = scores;
        log("scores reloaded.");
        this->reloadDisplay();
    }, true);
}

void Scoreboard::reloadDisplay() {
    if (this->scores == nullptr) {
        return;
    }
    Director::getInstance()->getScheduler()->performFunctionInCocosThread([this]{
        this->removeAllItems();
        float offset = 0;
        for(auto score : *this->scores)
        {
            auto str = score.first + ": " + std::to_string(score.second);
            auto label = Label::createWithTTF(str, "fonts/Marker Felt.ttf", 8);
            if (this->playerName == score.first) {
                label->setColor(Color3B::GREEN);
            }
            label->setAnchorPoint(Vec2::ZERO);
            label->setPosition(label->getPosition().x, label->getPosition().y + offset);
            offset += label->getContentSize().height;
            this->addChild(label);
        }
        this->setInnerContainerSize(Size(this->getContentSize().width, offset));
    });
}
