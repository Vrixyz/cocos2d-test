//
//  PlayerFish.hpp
//  test-mobile
//
//  Created by Thierry Berger on 23/11/2018.
//

#ifndef PlayerFish_hpp
#define PlayerFish_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "../Managers/BulletPool.h"

struct Command {
    cocos2d::Vec2 touch;
    bool isProcessed = true;
};

class PlayerFish;

class PlayerFishDelegate {
public:
    virtual void playerFishFinishedRotation(PlayerFish& playerFish) = 0;
};

class PlayerFish : public cocos2d::Sprite
{
    /// degrees per second
    const float rotationSpeed = 180.f;
    /// Time before each firing
    const float fireRate = 1.f;
    
    cocos2d::Vector<cocos2d::SpriteFrame*> fireFrames;
    cocos2d::Vector<cocos2d::SpriteFrame*> idleFrames;
    Command command;
    float angleTarget = 0;
    
    float currentReloadTime = 0;
    
    void updateRotation(float delta);
    cocos2d::Animation* createFireAnimation() const;
    cocos2d::Animation* createIdleAnimation() const;

public:
    PlayerFishDelegate *delegate = nullptr;
    
    PlayerFish();
    ~PlayerFish();
    static PlayerFish* create();
    
    void aim(const cocos2d::Vec2& target);
    void fire(BulletPool& bulletPool, const cocos2d::Vec2& target);
    void update(float) override;
};

#endif /* PlayerFish_hpp */
